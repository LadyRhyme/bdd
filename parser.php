<?php
require 'initTables.php';
require 'getData.php';
require 'display.php';

// Lié BDD

try {
    $bdd = new PDO('sqlite:data.db');
} catch (PDOException $exception) {
    echo 'Failed : ' . $exception->getMessage();
}

$getData = new getData();
$product = $getData->getProductData();
$order = $getData->getOrderData();
$customer = $getData->getCustomerData();

// Remplissage BDD

/*
$client = new fillDataBase();
$client->client($bdd, $customer);

$color = $client->notDuplicates($order);
$client->color($bdd, $color);

$vendor = $client->noDuplcates($product);
$client->vendor($bdd, $vendor);

$client->product($bdd, $product);

$client->order($bdd, $order);
*/

// Récup' BDD

$display = new display();
$arrayCustomer = $display->getCustomer($bdd);
$arrayProduct = $display->getProduct($bdd);
$arrayOrder = $display->getOrder($bdd);

// #Affichage des tableaux 

//$display->displayOrder($arrayOrder);
//$display->displayCustomer($arrayCustomer);
$display->displayProduct($arrayProduct);
<?php

    class fillDataBase {

        function client($bdd, $customer) {
            foreach ($customer as $key => $value) {
                $sql = "INSERT INTO client(nom, prenom, numero, mail, adresse, ville, pays) VALUES (:nom, :prenom, :numero, :mail, :adresse, :ville, :pays)";
                $res = $bdd->prepare($sql);
                $res->bindParam(':nom', $value['B']);
                $res->bindParam(':prenom', $value['C']);
                $res->bindParam(':numero', $value['E']);
                $res->bindParam(':mail', $value['D']);
                $res->bindParam(':adresse', $value['G']);
                $res->bindParam(':ville', $value['F']);
                $res->bindParam(':pays', $value['H']);
                $res->execute();
            }
        }

        function color($bdd, $arrayColor) {
            foreach ($arrayColor as $key => $value) {
                $sql = "INSERT INTO color(color) VALUES (:color)";
                $res = $bdd->prepare($sql);
                $res->bindParam(':color', $value);
                $res->execute();
            }
        }

        function notDuplicates($orderData) {
            $arrayColor = array();
            foreach ($orderData as $key => $order) {
                array_push($arrayColor, $order['E']);
            }
            return array_unique($arrayColor);
        }

        function vendor($bdd, $vendor) {
            foreach ($vendor as $key => $value) {
                $sql = "INSERT INTO vendor(vendor) VALUES (:vendor)";
                $res = $bdd->prepare($sql);
                $res->bindParam(':vendor', $value);
                $res->execute();
            }
        }

        function noDuplcates($productData)  {
            $arrayVendor = array();
            foreach ($productData as $key => $product) {
                array_push($arrayVendor, $product['C']);
            }
            return array_unique($arrayVendor);
        }

        function product($bdd, $product) {
            foreach ($product as $key => $product) { 
                $sql = "INSERT INTO produit(productName, vendorId, producturl, productPrice, productText, productTags) VALUES (:productName, :vendorId, :producturl, :productPrice, :productText, :productTags)";
                $res = $bdd->prepare($sql);
                $vendorId = $this->getVendorId($bdd, $product['C']);
                $res->bindParam(':productName', $product['B']);
                $res->bindParam(':vendorId', $vendorId);
                $res->bindParam(':producturl', $product['E']);
                $res->bindParam(':productPrice', $product['G']);
                $res->bindParam(':productText', $product['D']);
                $res->bindParam(':productTags', $product['F']);
                $res->execute();
            }
        }

        private function getVendorId($bdd, $vendor) {
            $sql = "SELECT vendorId FROM vendor WHERE vendor LIKE '%$vendor%'";
            $stmt = $bdd->query($sql);
            $array = $stmt->fetch();
            $id = $array["vendorId"];
            return $id;
        }

        function order($bdd, $order) {
            foreach ($order as $key => $value) {
                $sql = "INSERT INTO commande(commandeDate, productId, clientId, colorId) VALUES (:commandeDate, :productId, :clientId, :colorId)";
                $res = $bdd->prepare($sql);
                $colorid = $this->getColorId($bdd, $value['E']);
                $customerid = substr($value['B'], 9);
                $productid = substr($value['C'], 8);
                $res->bindParam(':commandeDate', $value['D']);
                $res->bindParam(':productId', $productid);
                $res->bindParam(':clientId', $customerid);
                $res->bindParam(':colorId', $colorid);
                $res->execute(); 
            }
        }

        private function getColorId($bdd, $color) {
            $sql = "SELECT colorId FROM color WHERE color LIKE '%$color%'";
            $stmt = $bdd->query($sql);
            $array = $stmt->fetch();
            $id = $array["colorId"];
            return $id;
        }
    }

?>
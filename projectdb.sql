-- Base de donnée --

-- Client lié à adresse"
CREATE TABLE "client" (
   clientId INTEGER PRIMARY KEY AUTOINCREMENT,
   nom VARCHAR(255),
   prenom VARCHAR(255),
   numero VARCHAR(255),
   mail VARCHAR(255),
   adresse VARCHAR(255),
   ville VARCHAR(255),
   pays VARCHAR(255)
);

-- Commande lié au client --
CREATE TABLE "commande" (
    commandeId INTEGER PRIMARY KEY AUTOINCREMENT,
    commandeDate DATE,
    productId INTEGER,
    clientId INTEGER,
    colorId INTEGER,
    FOREIGN KEY (productId) REFERENCES produit(productId),
    FOREIGN KEY (colorId) REFERENCES color (colorId),
    FOREIGN KEY (clientId) REFERENCES client(clientId)
);

-- Produits --
CREATE TABLE "produit" (
    productId INTEGER PRIMARY KEY AUTOINCREMENT,
    productName VARCHAR(255),
    productPrice FLOAT,
    productText VARCHAR(255),
    producturl VARCHAR(255),
    productTags VARCHAR(255),
    vendorId INTEGER,
    FOREIGN KEY (vendorId) REFERENCES vendor (vendorId)
);

-- Vendeur lié à Produit --
CREATE TABLE "vendor" (
    vendorId INTEGER PRIMARY KEY AUTOINCREMENT,
    vendor VARCHAR(255)
);

-- Color lié à Produit --
CREATE TABLE "color" (
    colorId INTEGER PRIMARY KEY AUTOINCREMENT,
    color VARCHAR(255)
);

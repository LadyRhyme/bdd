<?php

    require 'vendor/autoload.php';
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

    // Read XLSX  et génération tab

    class getData {

        private function getReader() {
            $reader = new Xlsx();
            return $reader->load("database.xlsx");
        }

        function getCustomerData() {
            $spreadsheet = $this->getReader();
            return $sheetCustomerData = $spreadsheet->getSheet(0)->toArray(NULL, TRUE, TRUE, TRUE);
        }

        function getProductData() {
            $spreadsheet = $this->getReader();
            return $sheetProductData = $spreadsheet->getSheet(2)->toArray(NULL, TRUE, TRUE, TRUE);
        }

        function getOrderData() {
            $spreadsheet = $this->getReader();
            return $sheetProductData = $spreadsheet->getSheet(1)->toArray(NULL, TRUE, TRUE, TRUE);
        }
    }

?>
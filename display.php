<?php

    // Fetch SQL

    class display {

        function getCustomer($bdd) {
            $sql = "SELECT clientId, nom, prenom, numero, mail, adresse, ville, pays FROM client";
            $stmt = $bdd->query($sql);
            $array = $stmt->fetchAll();
            return $array;    
        }

        function getProduct($bdd) {
            $sql = "SELECT productId, productName, productPrice, productText, producturl, productTags, vendor.vendor FROM produit LEFT JOIN vendor ON produit.vendorId = vendor.vendorId";
            $stmt = $bdd->query($sql);
            $array = $stmt->fetchAll();
            return $array;    
        }

        function getOrder($bdd) {
            $sql = "SELECT commandeId, commandeDate, produit.productName, client.nom, color.color FROM commande LEFT JOIN produit ON commande.productId = produit.productId LEFT JOIN color ON commande.colorId = color.colorId LEFT JOIN client ON commande.clientId = client.clientId";
            $stmt = $bdd->query($sql);
            $array = $stmt->fetchAll();
            return $array;    
        }
    
        // Affichage tableau

        function displayOrder($arrayOrder) {
            foreach ($arrayOrder as $key => $value) {
                ?>
                <div class="container">
                    <p><b>id:</b> <?=$value["commandeId"]?>| Date: <?= $value["commandeDate"]?>| name:<?= $value["productName"]?>| client: <?= $value["nom"]?>| couleur:<?= $value["color"]?></p>
                    <hr>
                </div>
                <?php
            }
        }

        function displayCustomer($arrayCustomer) {
            foreach ($arrayCustomer as $key => $value) {
                ?>
                <div class="container">
                    <p>id: <?=$value["clientId"]?>| Nom: <?= $value["nom"]?>| Prénom:<?= $value["prenom"]?>| Tel: <?= $value["numero"]?>| Mail:<?= $value["mail"]?>| Adresse:<?= $value["adresse"]?>| Ville:<?= $value["ville"]?>| Pays:<?= $value["pays"]?></p>
                    <hr>
                </div>
                <?php
            }
        }

        function displayProduct($arrayProduct) {
            foreach ($arrayProduct as $key => $value) {
                ?>
                <div class="container">
                    <p><b>id:</b> <?=$value["productId"]?>| Nom: <?= $value["productName"]?>| Prix:<?= $value["productPrice"]?>| Description: <?= $value["productText"]?>| url:<?= $value["producturl"]?>| Tags:<?= $value["productTags"]?>| Vendeur:<?= $value["vendor"]?></p>
                    <hr>
                </div>
                <?php
            }
        }
    }

?>